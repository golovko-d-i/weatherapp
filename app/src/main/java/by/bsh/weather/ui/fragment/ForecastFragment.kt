package by.bsh.weather.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import by.bsh.weather.R
import by.bsh.weather.data.BindableModel
import by.bsh.weather.data.feed.FiveDaysForecastFeed
import by.bsh.weather.data.feed.TodayForecastFeed
import by.bsh.weather.data.remote.model.X
import by.bsh.weather.databinding.ForecastFragmentBinding
import by.bsh.weather.databinding.ItemForecastBinding
import by.bsh.weather.di.Injectable
import by.bsh.weather.ui.view.BindableAdapter
import by.bsh.weather.ui.view.BindableHolder
import by.bsh.weather.ui.viewmodel.ForecastViewModel
import by.bsh.weather.ui.viewmodel.MainActivityViewModel
import by.bsh.weather.utils.getViewModel

class ForecastFragment : WeatherFragment() {

    private lateinit var binding: ForecastFragmentBinding

    private lateinit var viewModel: ForecastViewModel
    private var mainActivityViewModel: MainActivityViewModel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_forecast, container, false)
        return binding.root
    }

    override fun bindView(view: View, factory: ViewModelProvider.Factory, savedInstanceState: Bundle?) {
        viewModel = getViewModel(factory)
        binding.viewModel = viewModel
        binding.recyclerView.layoutManager = LinearLayoutManager(view.context)
        binding.recyclerView.adapter = FiveDaysAdapter()
        binding.lifecycleOwner = this
        activity?.let {
            mainActivityViewModel = it.getViewModel(factory)
            mainActivityViewModel?.locationLiveData?.observe(this, Observer { newLocation ->
                newLocation?.let { location ->
                    viewModel.load(location.latitude, location.longitude)
                }
            })
        }
    }

    override fun refresh() {
        viewModel.refresh()
    }

    private class FiveDaysAdapter: RecyclerView.Adapter<BindableHolder<BindableModel>>(), BindableAdapter<FiveDaysForecastFeed?> {

        private var data: FiveDaysForecastFeed? = null

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindableHolder<BindableModel> {
            val binding: ItemForecastBinding = ItemForecastBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return BindableHolder<X>(binding)
        }

        override fun onBindViewHolder(holder: BindableHolder<BindableModel>, position: Int) {
            holder.bind(data?.forecast(position))
        }

        override fun getItemCount(): Int = data?.size() ?: 0

        override fun setData(data: FiveDaysForecastFeed?) {
            this.data = data
            notifyDataSetChanged()
            // TODO diff util
        }
        override fun getData(): FiveDaysForecastFeed? = data

    }

}