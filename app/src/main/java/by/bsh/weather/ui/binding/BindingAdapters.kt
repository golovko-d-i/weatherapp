package by.bsh.weather.ui.binding

import android.preference.PreferenceManager
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import by.bsh.weather.R
import by.bsh.weather.ui.view.BindableAdapter
import by.bsh.weather.utils.fitAndCrop
import com.bumptech.glide.Glide
import java.text.DateFormat
import java.util.*

object BindingAdapters {

    @BindingAdapter("app:dateTime")
    @JvmStatic
    fun dateTime(view: TextView, oldTime: Int, newTime: Int) {
        if (oldTime != newTime) {
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = newTime.toLong()
            calendar.timeZone = TimeZone.getTimeZone("UTC")
            view.text = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(calendar.time)
        }
    }

    @BindingAdapter("app:time")
    @JvmStatic
    fun time(view: TextView, oldTime: Int, newTime: Int) {
        if (oldTime != newTime) {
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = newTime.toLong()
            calendar.timeZone = TimeZone.getTimeZone("UTC")
            view.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(calendar.time)
        }
    }

    @BindingAdapter("app:temperature")
    @JvmStatic
    fun tempFormat(view: TextView, temp: Double) {
        val pref = PreferenceManager.getDefaultSharedPreferences(view.context).getString("units_key", view.context.getString(R.string.metric_unit))
        view.text = "$temp$pref"
    }

    @BindingAdapter("app:weatherIcon")
    @JvmStatic
    fun weatherImage(view: ImageView, icon: String) {
        Glide.with(view)
            .load("https://openweathermap.org/img/w/$icon.png")
            .fitAndCrop()
            .into(view)
    }

    @BindingAdapter("data")
    @JvmStatic
    fun <T> setRecyclerViewProperties(recyclerView: RecyclerView, data: T?) {
        if (recyclerView.adapter is BindableAdapter<*>) {
            val a = recyclerView.adapter as BindableAdapter<T>
            if (a.getData() != data) {
                a.setData(data)
            }
        }
    }

}