package by.bsh.weather.ui.fragment

interface RefreshableFragment {

    fun refresh()

}