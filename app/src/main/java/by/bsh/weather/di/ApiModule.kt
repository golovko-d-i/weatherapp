package by.bsh.weather.di

import by.bsh.weather.WeatherApplication
import by.bsh.weather.data.remote.interceptor.ApiInterceptor
import by.bsh.weather.data.remote.interceptor.UnitsInterceptor
import by.bsh.weather.data.remote.WeatherApi
import by.bsh.weather.di.repository.FiveDaysWeatherRepository
import by.bsh.weather.di.repository.TodayWeatherRepository
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class ApiModule {

    /*
     * The method returns the Gson object
     * */
    @Provides
    @Singleton
    internal fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder()
        return gsonBuilder.create()
    }

    /*
     * The method returns the Cache object
     * */
    @Provides
    @Singleton
    internal fun provideCache(application: WeatherApplication): Cache {
        val cacheSize = (10 * 1024 * 1024).toLong() // 10 MB
        val httpCacheDirectory = File(application.cacheDir, "http-cache")
        return Cache(httpCacheDirectory, cacheSize)
    }

    /*
     * The method returns the UnitsInterceptor object
     * */
    @Provides
    @Singleton
    internal fun provideUnitsInterceptor(application: WeatherApplication): UnitsInterceptor {
        return UnitsInterceptor(application)
    }

    /*
     * The method returns the Okhttp object
     * */
    @Provides
    @Singleton
    internal fun provideOkhttpClient(cache: Cache, unitsInterceptor: UnitsInterceptor): OkHttpClient {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val httpClient = OkHttpClient.Builder()
        httpClient.cache(cache)
        httpClient.addNetworkInterceptor(ApiInterceptor())
        httpClient.addNetworkInterceptor(unitsInterceptor)
        httpClient.addInterceptor(logging)
        httpClient.connectTimeout(30, TimeUnit.SECONDS)
        httpClient.readTimeout(30, TimeUnit.SECONDS)
        return httpClient.build()
    }


    /*
     * The method returns the Retrofit object
     * */
    @Provides
    @Singleton
    internal fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .baseUrl("https://api.openweathermap.org/")
            .client(okHttpClient)
            .build()
    }

    /*
     * We need the WeatherApi module.
     * For this, We need the Retrofit object, Gson, Cache and OkHttpClient .
     * So we will define the providers for these objects here in this module.
     *
     * */
    @Provides
    @Singleton
    internal fun provideWeatherApi(retrofit: Retrofit): WeatherApi {
        return retrofit.create(WeatherApi::class.java)
    }

    /*
     * We need the TodayWeatherRepository.
     * For this, We need the WeatherApi.
     *
     * */
    @Provides
    @Singleton
    internal fun provideTodayWeatherApiRepository(weatherApi: WeatherApi): TodayWeatherRepository {
        return TodayWeatherRepository(weatherApi)
    }

    /*
     * We need the FiveDaysWeatherRepository.
     * For this, We need the WeatherApi.
     *
     * */
    @Provides
    @Singleton
    internal fun provideFiveDaysWeatherApiRepository(weatherApi: WeatherApi): FiveDaysWeatherRepository {
        return FiveDaysWeatherRepository(weatherApi)
    }

}