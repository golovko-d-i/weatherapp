package by.bsh.weather.ui.viewmodel

import androidx.lifecycle.ViewModel
import by.bsh.weather.utils.ifLet

abstract class LatLongViewModel: ViewModel() {

    private var latitude: Double? = null
    private var longitude: Double? = null

    fun load(latitude: Double?, longitude: Double?) {
        ifLet(latitude, longitude) {
            this.latitude = it[0]
            this.longitude = it[1]
            loadData(it[0], it[1])
        }
    }

    fun refresh() {
        ifLet(latitude, longitude) {
            loadData(it[0], it[1])
        }
    }

    protected abstract fun loadData(latitude: Double, longitude: Double)

}