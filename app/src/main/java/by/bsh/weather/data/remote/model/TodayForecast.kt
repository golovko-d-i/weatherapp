package by.bsh.weather.data.remote.model

import by.bsh.weather.data.BindableModel
import com.google.gson.annotations.SerializedName

data class TodayForecast(
    val city: City,
    val cnt: Int,
    val cod: String,
    val list: List<X>,
    val message: Double
)

data class X(
    val clouds: Clouds,
    val dt: Int,
    val dt_txt: String,
    val main: Main,
    val rain: Rain,
    val sys: Sys,
    val weather: List<Weather>,
    val wind: Wind
): BindableModel

data class Rain(
    @SerializedName("1h")
    val oneH: Double,
    @SerializedName("3h")
    val threeH: Double
)

data class Main(
    val grnd_level: Double,
    val humidity: Double,
    val pressure: Double,
    val sea_level: Double,
    val temp: Double,
    val temp_kf: Double,
    val temp_max: Double,
    val temp_min: Double
)

data class City(
    val coord: Coord,
    val country: String,
    val id: Int,
    val name: String
)

data class Coord(
    val lat: Double,
    val lon: Double
)