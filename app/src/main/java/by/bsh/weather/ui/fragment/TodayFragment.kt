package by.bsh.weather.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import by.bsh.weather.R
import by.bsh.weather.data.BindableModel
import by.bsh.weather.data.feed.TodayForecastFeed
import by.bsh.weather.data.remote.model.TodayWeather
import by.bsh.weather.data.remote.model.X
import by.bsh.weather.databinding.ItemForecastBinding
import by.bsh.weather.databinding.ItemNowBinding
import by.bsh.weather.databinding.TodayFragmentBinding
import by.bsh.weather.ui.view.BindableAdapter
import by.bsh.weather.ui.view.BindableHolder
import by.bsh.weather.ui.viewmodel.MainActivityViewModel
import by.bsh.weather.ui.viewmodel.TodayViewModel
import by.bsh.weather.utils.getViewModel

class TodayFragment : WeatherFragment() {

    private lateinit var binding: TodayFragmentBinding

    private lateinit var viewModel: TodayViewModel
    private var mainActivityViewModel: MainActivityViewModel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_today, container, false)
        return binding.root
    }

    override fun bindView(view: View, factory: ViewModelProvider.Factory, savedInstanceState: Bundle?) {
        viewModel = getViewModel(factory)
        binding.lifecycleOwner = this
        binding.recyclerView.layoutManager = LinearLayoutManager(context)
        binding.recyclerView.adapter = TodayAdapter()
        binding.viewModel = viewModel
        activity?.let {
            mainActivityViewModel = it.getViewModel(factory)
            mainActivityViewModel?.locationLiveData?.observe(this, Observer { newLocation ->
                newLocation?.let { location ->
                    viewModel.load(location.latitude, location.longitude)
                }
            })
        }
    }

    override fun refresh() {
        viewModel.refresh()
    }

    private class TodayAdapter : RecyclerView.Adapter<BindableHolder<BindableModel>>(),
        BindableAdapter<TodayForecastFeed?> {

        private var data: TodayForecastFeed? = null

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindableHolder<BindableModel> {
            val inflater = LayoutInflater.from(parent.context)
            return when (viewType) {
                TodayForecastFeed.TODAY -> {
                    val binding: ItemNowBinding = ItemNowBinding.inflate(inflater, parent, false)
                    BindableHolder<TodayWeather>(binding)
                }
                else -> {
                    val binding: ItemForecastBinding = ItemForecastBinding.inflate(inflater, parent, false)
                    BindableHolder<X>(binding)
                }
            }
        }

        override fun onBindViewHolder(holder: BindableHolder<BindableModel>, position: Int) {
            when (getItemViewType(position)) {
                TodayForecastFeed.TODAY -> holder.bind(data?.weather)
                TodayForecastFeed.FORECAST -> holder.bind(data?.hour(position))
            }
        }

        override fun getItemCount(): Int = data?.size() ?: 0
        override fun getItemViewType(position: Int): Int = data?.itemType(position) ?: TodayForecastFeed.TODAY
        override fun setData(data: TodayForecastFeed?) {
            this.data = data
            notifyDataSetChanged()
            // TODO diffutil
        }

        override fun getData(): TodayForecastFeed? = data

    }


}