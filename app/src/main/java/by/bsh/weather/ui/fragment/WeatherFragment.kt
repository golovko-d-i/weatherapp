package by.bsh.weather.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import by.bsh.weather.di.Injectable
import javax.inject.Inject

abstract class WeatherFragment : Fragment(), Injectable, RefreshableFragment {

    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory

    final override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindView(view, viewModelFactory, null)
    }

    protected abstract fun bindView(
        view: View,
        factory: ViewModelProvider.Factory,
        savedInstanceState: Bundle?
    )

}