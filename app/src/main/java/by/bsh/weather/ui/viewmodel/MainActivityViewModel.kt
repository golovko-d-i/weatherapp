package by.bsh.weather.ui.viewmodel

import android.Manifest
import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.text.format.DateUtils
import androidx.databinding.ObservableField
import androidx.lifecycle.*
import by.bsh.weather.R
import by.bsh.weather.WeatherApplication
import by.bsh.weather.ui.activity.MainActivity
import pub.devrel.easypermissions.EasyPermissions
import timber.log.Timber
import javax.inject.Inject

class MainActivityViewModel @Inject constructor(application: WeatherApplication) : AndroidViewModel(application),
    EasyPermissions.PermissionCallbacks {

    val hasPermissions = ObservableField<Boolean>(
        EasyPermissions.hasPermissions(
            getApplication(),
            Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION
        )
    )

    private val _location = MutableLiveData<Location?>()
    val locationLiveData: LiveData<Location?>
        get() = _location

    private val locationListener = object : LocationListener {

        override fun onLocationChanged(location: Location) {
            if (isBetterLocation(location, _location.value) && location != _location.value) {
                Timber.i("New location ${location == _location.value}")
                _location.value = location
            }
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {
        }

        override fun onProviderEnabled(provider: String) {
        }

        override fun onProviderDisabled(provider: String) {
        }
    }

    fun refresh(activity: MainActivity) {
        activity.refreshFragments()
    }

    fun requestPermissions(activity: MainActivity) {
        if (EasyPermissions.hasPermissions(
                getApplication(), Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        ) {
            hasPermissions.set(true)
            requestLocation()
        } else {
            hasPermissions.set(false)
            EasyPermissions.requestPermissions(
                activity,
                getApplication<WeatherApplication>().getString(R.string.location_rationale),
                REQUEST_PERMISSIONS,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun checkPermissions() {
        if (EasyPermissions.hasPermissions(
                getApplication(),
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        ) {
            hasPermissions.set(true)
            requestLocation()
        } else {
            hasPermissions.set(false)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    @SuppressLint("MissingPermission")
    fun stopLocationTracking() {
        val locationManager =
            getApplication<WeatherApplication>().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.removeUpdates(locationListener)
    }

    @SuppressLint("MissingPermission")
    private fun requestLocation() {
        val locationManager =
            getApplication<WeatherApplication>().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val locationProvider: String = LocationManager.GPS_PROVIDER

        val lastKnownLocation: Location = locationManager.getLastKnownLocation(locationProvider)
        _location.value = lastKnownLocation

        locationManager.requestLocationUpdates(locationProvider, DateUtils.MINUTE_IN_MILLIS * 15, 1000f, locationListener)
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        hasPermissions.set(false)
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        hasPermissions.set(true)
        requestLocation()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    /** Determines whether one Location reading is better than the current Location fix
     * @param location The new Location that you want to evaluate
     * @param currentBestLocation The current Location fix, to which you want to compare the new one
     */
    private fun isBetterLocation(location: Location, currentBestLocation: Location?): Boolean {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true
        }

        // Check whether the new location fix is newer or older
        val timeDelta: Long = location.time - currentBestLocation.time
        val isSignificantlyNewer: Boolean = timeDelta > TWO_MINUTES
        val isSignificantlyOlder: Boolean = timeDelta < -TWO_MINUTES

        when {
            // If it's been more than two minutes since the current location, use the new location
            // because the user has likely moved
            isSignificantlyNewer -> return true
            // If the new location is more than two minutes older, it must be worse
            isSignificantlyOlder -> return false
        }

        // Check whether the new location fix is more or less accurate
        val isNewer: Boolean = timeDelta > 0L
        val accuracyDelta: Float = location.accuracy - currentBestLocation.accuracy
        val isLessAccurate: Boolean = accuracyDelta > 0f
        val isMoreAccurate: Boolean = accuracyDelta < 0f
        val isSignificantlyLessAccurate: Boolean = accuracyDelta > 200f

        // Check if the old and new location are from the same provider
        val isFromSameProvider: Boolean = location.provider == currentBestLocation.provider

        // Determine location quality using a combination of timeliness and accuracy
        return when {
            isMoreAccurate -> true
            isNewer && !isLessAccurate -> true
            isNewer && !isSignificantlyLessAccurate && isFromSameProvider -> true
            else -> false
        }
    }

    companion object {
        private const val REQUEST_PERMISSIONS = 845
        private const val TWO_MINUTES: Long = 1000 * 60 * 2
    }

}