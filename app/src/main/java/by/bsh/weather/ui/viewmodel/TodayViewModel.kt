package by.bsh.weather.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import by.bsh.weather.data.feed.TodayForecastFeed
import by.bsh.weather.di.repository.TodayWeatherRepository
import io.reactivex.disposables.Disposable
import timber.log.Timber
import javax.inject.Inject

class TodayViewModel @Inject constructor(private val weatherApiRepository: TodayWeatherRepository) : LatLongViewModel() {

    private var disposable: Disposable? = null
    private val _todayForecastLiveData = MutableLiveData<TodayForecastFeed>()
    val todayForecastLiveData: LiveData<TodayForecastFeed>
        get() = _todayForecastLiveData

    override fun loadData(latitude: Double, longitude: Double) {
        disposable = weatherApiRepository.todayWeatherFeed(latitude, longitude)
            .subscribe(
                {
                    _todayForecastLiveData.postValue(it)
                },
                {
                    Timber.e(it)
                })
    }

    override fun onCleared() {
        super.onCleared()
        disposable?.dispose()
    }

}