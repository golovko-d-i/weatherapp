package by.bsh.weather.ui.view

interface BindableAdapter<T> {
    fun setData(data: T?)
    fun getData(): T?
}