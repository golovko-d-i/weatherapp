package by.bsh.weather.di

import by.bsh.weather.ui.fragment.ForecastFragment
import by.bsh.weather.ui.fragment.TodayFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeTodayFragment(): TodayFragment

    @ContributesAndroidInjector
    abstract fun contributeForecastFragment(): ForecastFragment

}