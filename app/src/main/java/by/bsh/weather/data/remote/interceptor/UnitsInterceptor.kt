package by.bsh.weather.data.remote.interceptor

import android.content.Context
import android.preference.PreferenceManager
import by.bsh.weather.R
import okhttp3.Interceptor
import okhttp3.Response
import timber.log.Timber

class UnitsInterceptor constructor(context: Context) : Interceptor {

    private var unit = PreferenceManager.getDefaultSharedPreferences(context)
        .getString(
            context.getString(R.string.pref_units_key),
            context.getString(R.string.pref_unit_metric)
        )

    init {
        PreferenceManager.getDefaultSharedPreferences(context)
            .registerOnSharedPreferenceChangeListener { prefs, key ->
                if (key == context.getString(R.string.pref_units_key)) {
                    unit = prefs.getString(
                        context.getString(R.string.pref_units_key),
                        context.getString(R.string.pref_unit_metric)
                    )
                }
            }
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val originalUrl = originalRequest.url()
        val url = originalUrl.newBuilder()
            .addQueryParameter("units", unit)
            .build()

        val requestBuilder = originalRequest.newBuilder().url(url)
        val request = requestBuilder.build()
        return chain.proceed(request)
    }
}