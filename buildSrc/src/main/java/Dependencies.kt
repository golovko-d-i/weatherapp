/*
 * Copyright (c) 2019. ChiliMobili
 * All rights reserved.
 */

object ApplicationId {
    val id = "by.bsh.weather"
}

object Modules {
    val app = ":app"
}

object Releases {
    val versionCode = 34
    val versionName = "1.0.6"
}

object Versions {
    val kotlin = "1.3.30"
    val gradle = "3.4.0"
    val compileSdk = 28
    val minSdk = 21
    val targetSdk = 28
    val appCompat = "1.0.2"
    val coreKtx = "1.0.0"
    val constraintLayout = "1.1.3"
    val junit = "4.12"
    val androidTestRunner = "1.1.2-alpha02"
    val espressoCore = "3.2.0-alpha02"
    val retrofit = "2.5.0"
    val retrofitCoroutines = "0.9.2"
    val retrofitGson = "2.4.0"
    val gson = "2.8.5"
    val okHttp = "3.12.1"
    val coroutines = "1.1.1"
    val koin = "1.0.2"
    val timber = "4.7.1"
    val lifecycle = "2.0.0"
    val reactiveStreams = "1.1.1"
    val nav = "2.0.0"
    val room = "2.1.0-alpha06"
    val recyclerview = "1.0.0"
    val safeArgs = "2.1.0-alpha01"
    val glide = "4.9.0"
    val glideTransformations = "4.0.0"
    val mockwebserver = "2.7.5"
    val archCoreTest = "2.0.0"
    val androidJunit = "1.1.0"
    val mockk = "1.9.2"
    val fragmentTest = "1.1.0-alpha06"
    val databinding = "3.3.2"
    val material = "1.0.0"
    val rxAndroid = "2.1.0"
    val rxJava = "2.2.2"
    val materialCalendar = "2.0.0"
    val paging = "2.1.0"
    val crashlytics = "2.9.9@aar"
    val rxBilling = "0.9.2"
    val billingClient = "1.2"
    val firebaseCore = "16.0.8"
    val firebasePerf = "16.2.5"
    val firebaseConfig = "16.5.0"
    val firebaseInappmessaging = "17.1.1"
    val firebaseMessaging = "17.6.0"
    val firebaseAds = "17.2.0"
    val playServicesBase = "16.1.0"
    val facebook = "[4,5)"
    val audienceNetwork = "5.2.0"
    val flurry = "11.4.0@aar"
    val myTarget = "5.2.5"
    val gdprConsent = "1.0.7"
    val dagger = "2.22.1"
    val easyPermission = "3.0.0"
}

object Libraries {
    // KOIN
    val koin = "org.koin:koin-android:${Versions.koin}"
    val koinViewModel = "org.koin:koin-android-viewmodel:${Versions.koin}"
    // ROOM
    val roomCompiler = "androidx.room:room-compiler:${Versions.room}"
    val roomRunTime = "androidx.room:room-runtime:${Versions.room}"
    val roomRxJava = "androidx.room:room-rxjava2:${Versions.room}"
    val roomKtx = "androidx.room:room-ktx:${Versions.room}"
    // RETROFIT
    val retrofitCoroutineAdapter = "com.jakewharton.retrofit:retrofit2-kotlin-coroutines-adapter:${Versions.retrofitCoroutines}"
    val gson = "com.google.code.gson:gson:${Versions.gson}"
    val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    val retrofitGsonConverter = "com.squareup.retrofit2:converter-gson:${Versions.retrofitGson}"
    val retrofitAdapterRxJava = "com.squareup.retrofit2:adapter-rxjava2:${Versions.retrofitGson}"
    val httpLoggingInterceptor = "com.squareup.okhttp3:logging-interceptor:${Versions.okHttp}"
    // RXJAVA
    val rxAndroid = "io.reactivex.rxjava2:rxandroid:${Versions.rxAndroid}"
    val rxJava = "io.reactivex.rxjava2:rxjava:${Versions.rxJava}"
    // GLIDE
    val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
    val glideKapt = "com.github.bumptech.glide:compiler:${Versions.glide}"
    val glideTransformations = "jp.wasabeef:glide-transformations:${Versions.glideTransformations}"
    // MATERIAL CALENDAR VIEW
    val materialCalendar = "com.github.prolificinteractive:material-calendarview:${Versions.materialCalendar}"
    // PAGING
    val paging = "androidx.paging:paging-runtime:${Versions.paging}"
    val pagingRxJava = "androidx.paging:paging-rxjava2:${Versions.paging}"
    // CRASHLITYCS
    val crashlytics = "com.crashlytics.sdk.android:crashlytics:${Versions.crashlytics}"
    // BILLING
    val rxBilling = "com.betterme:rxbilling:${Versions.rxBilling}"
    val billingClient = "com.android.billingclient:billing:${Versions.billingClient}"
    // TIMBER
    val timber = "com.jakewharton.timber:timber:${Versions.timber}"
    // FACEBOOK
    val facebook = "com.facebook.android:facebook-android-sdk:${Versions.facebook}"
    val audienceNetwork = "com.facebook.android:audience-network-sdk:${Versions.audienceNetwork}"
    val audienceNetworkMediation = "com.google.ads.mediation:facebook:${Versions.audienceNetwork}.0"
    // FLURRY
    val flurry = "com.flurry.android:analytics:${Versions.flurry}"
    // MYTARGET
    val myTarget = "com.my.target:mytarget-sdk:${Versions.myTarget}"
    val myTargetMediation = "com.google.ads.mediation:mytarget:${Versions.myTarget}.0"
    // DAGGER
    val daggerRuntime = "com.google.dagger:dagger:${Versions.dagger}"
    val dagger = "com.google.dagger:dagger-android:${Versions.dagger}"
    val daggerAndroid = "com.google.dagger:dagger-android-support:${Versions.dagger}"
    val daggerCompiler = "com.google.dagger:dagger-compiler:${Versions.dagger}"
    val daggerAndroidProcessor = "com.google.dagger:dagger-android-processor:${Versions.dagger}"
    // EASY PERMISSIONS
    val easyPermissions = "pub.devrel:easypermissions:${Versions.easyPermission}"
}

object KotlinLibraries {
    val kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"
    val kotlinCoroutineCore = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutines}"
}

object AndroidLibraries {
    // KOTLIN
    val kotlinCoroutineAndroid = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutines}"
    // ANDROID
    val appCompat = "androidx.appcompat:appcompat:${Versions.appCompat}"
    val coreKtx = "androidx.core:core-ktx:${Versions.coreKtx}"
    val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"

    val lifecycleRuntime = "androidx.lifecycle:lifecycle-runtime:${Versions.lifecycle}"
    val lifecycleExtensions = "androidx.lifecycle:lifecycle-extensions:${Versions.lifecycle}"
    val lifecycleViewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle}"
    val lifecycleCompiler = "androidx.lifecycle:lifecycle-compiler:${Versions.lifecycle}"
    val lifecycleJava8  = "androidx.lifecycle:lifecycle-common-java8:${Versions.lifecycle}"
    val lifecycleReactiveStreams = "android.arch.lifecycle:reactivestreams:${Versions.reactiveStreams}"
    val lifecycleArchExtensions = "android.arch.lifecycle:extensions:${Versions.reactiveStreams}"

    val recyclerView = "androidx.recyclerview:recyclerview:${Versions.recyclerview}"
    val navigation = "androidx.navigation:navigation-ui-ktx:${Versions.nav}"
    val navigationFrag = "androidx.navigation:navigation-fragment-ktx:${Versions.nav}"
    val material = "com.google.android.material:material:${Versions.material}"
    val firebaseCore = "com.google.firebase:firebase-core:${Versions.firebaseCore}"
    val firebasePerf = "com.google.firebase:firebase-perf:${Versions.firebasePerf}"
    val firebaseConfig = "com.google.firebase:firebase-config:${Versions.firebaseConfig}"
    val firebaseInappmessaging = "com.google.firebase:firebase-inappmessaging-display:${Versions.firebaseInappmessaging}"
    val firebaseMessaging = "com.google.firebase:firebase-messaging:${Versions.firebaseMessaging}"
    val firebaseAds = "com.google.firebase:firebase-ads:${Versions.firebaseAds}"
    val playServicesBase = "com.google.android.gms:play-services-base:${Versions.playServicesBase}"
    val gdprConsent = "com.google.android.ads.consent:consent-library:${Versions.gdprConsent}"
}

object TestLibraries {
    // ANDROID TEST
    val androidTestRunner = "androidx.test:runner:${Versions.androidTestRunner}"
    val espresso = "androidx.test.espresso:espresso-core:${Versions.espressoCore}"
    val espressoContrib = "androidx.test.espresso:espresso-contrib:${Versions.espressoCore}"
    val archCoreTest = "androidx.arch.core:core-testing:${Versions.archCoreTest}"
    val junit = "androidx.test.ext:junit:${Versions.androidJunit}"
    val fragmentNav = "androidx.fragment:fragment-testing:${Versions.fragmentTest}"
    // KOIN
    val koin = "org.koin:koin-test:${Versions.koin}"
    // MOCK WEBSERVER
    val mockWebServer = "com.squareup.okhttp:mockwebserver:${Versions.mockwebserver}"
    // MOCK
    val mockkAndroid = "io.mockk:mockk-android:${Versions.mockk}"
    val mockk = "io.mockk:mockk:${Versions.mockk}"
    // COROUTINE
    val coroutine = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.coroutines}"
    // DATA BINDING
    val databinding = "androidx.databinding:databinding-compiler:${Versions.databinding}"
}