package by.bsh.weather.data.remote.model

data class FiveDaysForecast(
    val city: City,
    val cnt: Int,
    val cod: String,
    val list: List<X>,
    val message: Double
)