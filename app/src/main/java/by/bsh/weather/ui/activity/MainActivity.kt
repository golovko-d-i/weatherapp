package by.bsh.weather.ui.activity

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import by.bsh.weather.R
import by.bsh.weather.databinding.MainActivityBinding
import by.bsh.weather.ui.fragment.ForecastFragment
import by.bsh.weather.ui.fragment.RefreshableFragment
import by.bsh.weather.ui.fragment.TodayFragment
import by.bsh.weather.ui.viewmodel.MainActivityViewModel
import by.bsh.weather.utils.getViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject


class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener,
    HasSupportFragmentInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>
    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var binding: MainActivityBinding
    lateinit var viewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = getViewModel(viewModelFactory)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.activity = this
        binding.viewmodel = viewModel
        binding.bottomNavigation.setOnNavigationItemSelectedListener(this)

        binding.viewPager.adapter = ForecastPagerAdapter(supportFragmentManager)
        binding.viewPager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {

            private var prevMenuItem: MenuItem? = null

            override fun onPageSelected(position: Int) {
                if (prevMenuItem != null) {
                    prevMenuItem?.isChecked = false
                } else {
                    binding.bottomNavigation.menu.getItem(0).isChecked = false
                }

                binding.bottomNavigation.menu.getItem(position).isChecked = true
                prevMenuItem = binding.bottomNavigation.menu.getItem(position)
            }
        })
        viewModel.requestPermissions(this)
//        savedInstanceState?.also {
////            binding.viewPager.currentItem = it.getInt(PAGE)
//        }
    }

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    fun refreshFragments() {
        supportFragmentManager.fragments.forEach {
            if (it is RefreshableFragment) {
                it.refresh()
            }
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.today -> binding.viewPager.currentItem = 0
            R.id.forecast -> binding.viewPager.currentItem = 1
        }
        return false
    }

//    override fun onSaveInstanceState(outState: Bundle?) {
//        super.onSaveInstanceState(outState)
//        outState?.also {
//            it.putInt(PAGE, binding.viewPager.currentItem)
//        }
//    }

    private class ForecastPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            return if (position == 0) {
                TodayFragment()
            } else {
                ForecastFragment()
            }
        }

        override fun getCount(): Int = 2
    }

    companion object {
        private const val PAGE = "page"
    }
}
