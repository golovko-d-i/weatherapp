package by.bsh.weather.data.feed

import by.bsh.weather.data.remote.model.FiveDaysForecast
import by.bsh.weather.data.remote.model.X

class FiveDaysForecastFeed(fiveDaysForecast: FiveDaysForecast) {

    private val forecastList: List<X> = fiveDaysForecast.list

    fun forecast(pos: Int) = forecastList[pos]
    fun size() = forecastList.size

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as FiveDaysForecastFeed

        if (forecastList != other.forecastList) return false

        return true
    }

    override fun hashCode(): Int {
        return forecastList.hashCode()
    }

    override fun toString(): String {
        return "FiveDaysForecastFeed(forecastList=$forecastList)"
    }

}