package by.bsh.weather.data.remote.model

import by.bsh.weather.data.BindableModel

data class TodayWeather(
    val base: String,
    val clouds: Clouds,
    val cod: Int,
    val coordinates: Coordinates,
    val dt: Int,
    val id: Int,
    val main: Main,
    val name: String,
    val sys: Sys,
    val visibility: Int,
    val weather: List<Weather>,
    val wind: Wind
): BindableModel

data class Wind(
    val deg: Double,
    val speed: Double
)

data class Weather(
    val description: String,
    val icon: String,
    val id: Int,
    val main: String
)

data class Sys(
    val country: String,
    val id: Int,
    val message: Double,
    val sunrise: Int,
    val sunset: Int,
    val type: Int,
    val pod: String
)

data class Clouds(
    val all: Int
)

data class Coordinates(
    val lat: Double,
    val lon: Double
)