package by.bsh.weather.data.remote

import by.bsh.weather.data.remote.model.FiveDaysForecast
import by.bsh.weather.data.remote.model.TodayForecast
import by.bsh.weather.data.remote.model.TodayWeather
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

    @GET("/data/2.5/weather")
    fun currentWeather(@Query(value = "lat") latitude: Double, @Query(value = "lon") longitude: Double): Single<TodayWeather>

    @GET("/data/2.5/forecast/hourly")
    fun hourlyForecast(@Query(value = "lat") latitude: Double, @Query(value = "lon") longitude: Double): Single<TodayForecast>

    @GET("data/2.5/forecast")
    fun fiveDaysForecast(@Query(value = "lat") latitude: Double, @Query(value = "lon") longitude: Double): Single<FiveDaysForecast>

}