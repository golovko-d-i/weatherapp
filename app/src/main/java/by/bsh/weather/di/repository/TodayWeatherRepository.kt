package by.bsh.weather.di.repository

import by.bsh.weather.data.feed.TodayForecastFeed
import by.bsh.weather.data.remote.model.TodayForecast
import by.bsh.weather.data.remote.model.TodayWeather
import by.bsh.weather.data.remote.WeatherApi
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TodayWeatherRepository @Inject constructor(private val weatherApi: WeatherApi) {

    fun todayWeatherFeed(latitude: Double, longitude: Double): Single<TodayForecastFeed> {
        return currentWeather(latitude, longitude)
            .zipWith(hourlyForecast(latitude, longitude),
                BiFunction<TodayWeather, TodayForecast, TodayForecastFeed> { weather, forecast ->
                    TodayForecastFeed(
                        weather,
                        forecast
                    )
                })
    }

    private fun currentWeather(latitude: Double, longitude: Double): Single<TodayWeather> =
        weatherApi.currentWeather(latitude, longitude)

    private fun hourlyForecast(latitude: Double, longitude: Double): Single<TodayForecast> =
        weatherApi.hourlyForecast(latitude, longitude)

}