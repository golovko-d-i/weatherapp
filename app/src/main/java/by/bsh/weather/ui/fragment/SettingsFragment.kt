package by.bsh.weather.ui.fragment

import android.os.Bundle
import android.preference.PreferenceFragment
import by.bsh.weather.R

@Suppress("DEPRECATION")
class SettingsFragment: PreferenceFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addPreferencesFromResource(R.xml.settings)
    }

}