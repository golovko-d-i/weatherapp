package by.bsh.weather.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import by.bsh.weather.data.feed.FiveDaysForecastFeed
import by.bsh.weather.di.repository.FiveDaysWeatherRepository
import io.reactivex.disposables.Disposable
import timber.log.Timber
import javax.inject.Inject

class ForecastViewModel @Inject constructor(private val weatherApiRepository: FiveDaysWeatherRepository) :
    LatLongViewModel() {

    private var disposable: Disposable? = null
    private val _weatherData = MutableLiveData<FiveDaysForecastFeed>()
    val weatherData: LiveData<FiveDaysForecastFeed>
        get() = _weatherData

    override fun loadData(latitude: Double, longitude: Double) {
        disposable = weatherApiRepository.fiveDaysWeatherFeed(latitude, longitude)
            .subscribe(
                { _weatherData.postValue(it) },
                { Timber.e(it) }
            )
    }

    override fun onCleared() {
        super.onCleared()
        disposable?.dispose()
    }

}