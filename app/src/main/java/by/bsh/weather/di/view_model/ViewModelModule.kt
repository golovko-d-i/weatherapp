package by.bsh.weather.di.view_model

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import by.bsh.weather.ui.viewmodel.ForecastViewModel
import by.bsh.weather.ui.viewmodel.MainActivityViewModel
import by.bsh.weather.ui.viewmodel.TodayViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(TodayViewModel::class)
    abstract fun todayViewModel(todayViewModel: TodayViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ForecastViewModel::class)
    abstract fun forecastViewModel(forecastViewModel: ForecastViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainActivityViewModel::class)
    abstract fun mainActivityViewModel(mainActivityViewModel: MainActivityViewModel): ViewModel

    @Binds
    internal abstract fun bindViewModelFactory(factory: WeatherViewModelFactory): ViewModelProvider.Factory
}