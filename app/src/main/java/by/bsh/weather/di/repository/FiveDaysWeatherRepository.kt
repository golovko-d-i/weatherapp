package by.bsh.weather.di.repository

import by.bsh.weather.data.feed.FiveDaysForecastFeed
import by.bsh.weather.data.remote.model.FiveDaysForecast
import by.bsh.weather.data.remote.WeatherApi
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FiveDaysWeatherRepository @Inject constructor(private val weatherApi: WeatherApi) {

    fun fiveDaysWeatherFeed(latitude: Double, longitude: Double): Single<FiveDaysForecastFeed> =
        fiveDaysWeather(latitude, longitude)
            .map { FiveDaysForecastFeed(it) }

    private fun fiveDaysWeather(latitude: Double, longitude: Double): Single<FiveDaysForecast> =
        weatherApi.fiveDaysForecast(latitude, longitude)

}