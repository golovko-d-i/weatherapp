package by.bsh.weather.data

/**
 * Defines that model can be bound via binding adapter
 */
interface BindableModel