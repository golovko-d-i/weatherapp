package by.bsh.weather.data.feed

import android.util.SparseArray
import android.util.SparseIntArray
import by.bsh.weather.data.remote.model.TodayForecast
import by.bsh.weather.data.remote.model.TodayWeather
import by.bsh.weather.data.remote.model.X


class TodayForecastFeed(val weather: TodayWeather?, hourlyForecast: TodayForecast?) {

    private val itemTypes = SparseIntArray()
    private val hours = SparseArray<X>()

    fun hour(pos: Int): X = hours[pos]
    fun size() = itemTypes.size()
    fun itemType(pos: Int) = itemTypes[pos]

    init {
        weather?.also {
            itemTypes.put(size(), TODAY)
        }
        hourlyForecast?.also {
            it.list.forEach { x ->
                hours.put(size(), x)
                itemTypes.put(size(), FORECAST)
            }
        }
    }

    override fun toString(): String {
        return "TodayForecastFeed(weather=$weather, itemTypes=$itemTypes, hours=$hours)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TodayForecastFeed

        if (weather != other.weather) return false
        if (itemTypes != other.itemTypes) return false
        if (hours != other.hours) return false

        return true
    }

    override fun hashCode(): Int {
        var result = weather?.hashCode() ?: 0
        result = 31 * result + itemTypes.hashCode()
        result = 31 * result + hours.hashCode()
        return result
    }

    companion object {
        const val TODAY = 0
        const val FORECAST = 1
    }

}