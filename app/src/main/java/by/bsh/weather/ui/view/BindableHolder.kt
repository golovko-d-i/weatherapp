package by.bsh.weather.ui.view

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import by.bsh.weather.BR

class BindableHolder<out T>(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: Any?) {
        item?.also {
            binding.setVariable(BR.obj, item as T)
            binding.executePendingBindings()
        }
    }

}